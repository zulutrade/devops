**Welcome to ZuluTrade Assignments**
======================================================

***DevOps Assignment 1***
-

##### Tools you need to install:
java (openjdk 1.8), ansible, git

##### 1. First of all you need to commit and push the src code of devops.zip (located in this folder) into this repository (in a branch called devops_assignment1) so that one later could clone and build the project by just executing:

```bash
git clone https://gitlab.com/zulutrade/devops.git
cd devops && git checkout devops_assignment1
git pull origin devops_assignment1
mvn clean install package
mvn spring-boot:run
```

##### 2. This Spring-Boot project is built using Apache Maven (mvn)

You need to create a Gitlab CI job, so each time developers commit and push changes to the project a new Gitlab CI pipeline runs the unit tests and ensures that nothing has broken.

##### 3. Dockerfile

You need to create a Dockerfile that would checkout this project, builds it and runs it. You should create a new ansible role/playbook to create the docker container. You should commit and push this under a new folder of the project.

##### 4. Monitoring

When this project runs it exposes a health interface under: http://127.0.0.1:8080/actuator and responds with JSON.
You may use whatever tool you want to monitor:

- this process health: http://127.0.0.1:8080/actuator/health
- CPU of the system: http://127.0.0.1:8080/actuator/metrics/system.cpu.usage

If process is unhealthy or CPU usage is more than 10% send a mail to noreply@zulutrade.com.
It's up to you where this script would run and how but you should document the procedure in a README file.


Please write documentation wherever is applicable or you believe it is needed.
##### Good Luck!
